# Dokuwiki 1.1

This runs a dokuwiki server. It consists of one containers (dokuwiki).

Since Dokuwiki stores its data in files, the neccessary folder is mounted via a host volume mount.

## Getting Started

Copy the `.env-example` file into a `.env` file and edit the variables to fit your environment.

    # cp .env-example .env
    # vi .env

Start the container with the command:

    # sudo docker-compose up -d

Your Dokuwiki service is available on `http://127.0.0.1:8080` - you should put a reverse proxy like nginx upfront.

### Build / Extend

You can eg. add more volume mounts as you need.

### Replicating the service

Replicating should be easy via copying everything and adapting the `.env` variables to your liking

### Upgrading the service / containers

For upgrading the container, you will need to call

	# docker-compose down; docker-compose pull; docker-compose up -d

this tears down the old container, pulls the latest version and starts the new container.

## About the project

### Versioning

We use MAIN.MINOR number for versioning where as MAIN usually means big / breaking changes and MINOR usually means small / non breaking changes. For the version number, see at the top header or in the [tags on this repository](https://bitbucket.org/mmprivat/dokuwiki/downloads/?tab=tags).

### Author(s)

* **Manuel Manhart** - *Initial work*

### License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

### Contributing

If you want to contribute, contact the author or create a pull request.

### Changelog

__1.1__

* Rework on docker-compose to work with latest version of dokuwiki

__1.0__

* Added README
* Added LICENSE
* Put into public bitbucket repo
* Extracted settings to `.env` file

### Open issues

-

## Sources

* [Docker](http://www.docker.io/) - The container base
* [Docker-Compose](https://docs.docker.com/compose/) - Composing multiple containers into one service
* [VS Code](https://code.visualstudio.com/) - Used to edit all the files
* [Dokuwiki Image](https://hub.docker.com/r/bitnami/dokuwiki)

